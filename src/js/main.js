document.addEventListener('DOMContentLoaded', () => {
    const header = document.getElementById('header');
    const navItems = document.querySelectorAll('.nav-item');
    
    window.addEventListener('scroll', () => {
        // Handle navbar resizing
        if (window.scrollY > 100) { // Adjust as needed
            header.classList.add('small-nav');
        } else {
            header.classList.remove('small-nav');
        }
        
        let currentSection = 'home'; // Default section
        navItems.forEach(item => {
            const targetId = item.dataset.target;
            const targetElement = document.getElementById(targetId);
            //console.log(targetId, targetElement);  // Print out the ID and the corresponding element
            if (targetElement && targetElement.offsetTop <= window.scrollY) {
                currentSection = targetId;
            }
        });
        
        navItems.forEach(item => {
            if (item.dataset.target === currentSection) {
                item.classList.add('active-nav-item'); // Apply the active class to the current nav item
            } else {
                item.classList.remove('active-nav-item'); // Remove the active class from other nav items
            }
        });
    });
    
    // Handle smooth scrolling
    navItems.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault();
            const targetId = item.dataset.target;
            const targetElement = document.getElementById(targetId);
            
            window.scrollTo({
                top: targetElement.offsetTop - header.offsetHeight, // Adjust the scrolling position to below the header
                behavior: 'smooth'
            });
        });
    });
});

document.addEventListener('DOMContentLoaded', () => {
    const slides = document.querySelectorAll('#carousel .slide');
    let currentSlide = 0;
    
    // Handle the functionality for the next button
    document.getElementById('nextBtn').addEventListener('click', () => {
        goToSlide(currentSlide+1);
    });
    
    // Handle the functionality for the previous button
    document.getElementById('prevBtn').addEventListener('click', () => {
        goToSlide(currentSlide-1);
    });
    
    function goToSlide(n) {
        slides[currentSlide].classList.remove('active');
        currentSlide = (n + slides.length) % slides.length; // Ensure it's circular
        slides[currentSlide].classList.add('active');
    }
});

document.addEventListener('DOMContentLoaded', () => {
    const openModalBtns = document.querySelectorAll('.open-modal-btn');
    const closeModalBtns = document.querySelectorAll('.close-modal-btn');
    
    // When the user clicks a button, open the corresponding modal 
    openModalBtns.forEach(button => {
        button.addEventListener('click', function() {
            const modalId = this.getAttribute('data-modal');
            const modal = document.getElementById(modalId);
            modal.style.display = "block";
        });
    });
    
    // When the user clicks on <span> (x), close the modal
    closeModalBtns.forEach(button => {
        button.addEventListener('click', function() {
            this.closest('.modal').style.display = "none";
        });
    });
    
    // When the user clicks anywhere outside of the modal, close it
    window.addEventListener('click', function(event) {
        if (event.target.classList.contains('modal')) {
            event.target.style.display = "none";
        }
    });
});

document.addEventListener('DOMContentLoaded', () => {
    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                // Element is in the viewport
                entry.target.classList.add('in-viewport');
                entry.target.classList.remove('out-viewport'); // Optionally remove the out-viewport class if added previously
            } else {
                // Element is out of the viewport
                entry.target.classList.remove('in-viewport');
                entry.target.classList.add('out-viewport'); // Optionally add a class for out of viewport state
            }
        });
    });

    // Apply observer to all elements with the 'animated-element' class
    document.querySelectorAll('.animated-element').forEach(element => {
        observer.observe(element);
    });
});
